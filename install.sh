#!/bin/bash -x
GOOGLE_APPLICATION_CREDENTIALS="$HOME/.gcp/jsandlin-admin.json"
GITLAB_CREDS="$HOME/.gitlab/.creds"

 if [[ ! -f "$GOOGLE_APPLICATION_CREDENTIALS" ]]
 then
   echo "Cannot find Google App Creds file: " + $GOOGLE_APPLICATION_CREDENTIALS
   return 1
 fi

 if [[ ! -f "$GITLAB_CREDS" ]]
 then
   echo "Cannot find GitLab Creds file: " + $GITLAB_CREDS
   return 1
 fi

export GOOGLE_APPLICATION_CREDENTIALS
export GITLAB_CREDS

# DEBUG OPTION
#export TF_LOG=TRACE

IP=`curl ifconfig.me`
PLAN_FILE=myplan.tfplan
rm -fr .terraform
rm -f $PLAN_FILE


terraform init -backend-config=$GITLAB_CREDS
terraform plan -out $PLAN_FILE

if [[ "$1" == "create" ]]
then
  terraform apply --auto-approve
elif [[ "$1" == "destroy" ]]
then
  terraform destroy --auto-approve
fi