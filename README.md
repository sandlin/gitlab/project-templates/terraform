# __PROJECT NAME__

![Project Stage][project-stage-shield]
![Maintenance][maintenance-shield]
![Pipeline Status][pipeline-shield]

[![License][license-shield]](LICENSE.md)
[![Maintaner][maintainer-shield]](https://gitlab.com/jsandlin)

## Table of Contents
[[_TOC_]]

## Usage
In order to utilize the pre-commit-hook, once you clone your repo, execute `pre-commit install`

####

<!-- Let's define some variables for ease of use. -->
[project-stage-shield]: https://img.shields.io/badge/project%20stage-development-yellowgreen.svg
[issue]: https://gitlab.com/sandlin/project_templates/common/issues
[license-shield]: https://img.shields.io/github/license/hassio-addons/repository-beta.svg
[maintenance-shield]: https://img.shields.io/maintenance/yes/2021.svg
[maintainer-shield]: https://img.shields.io/badge/maintainer-James_Sandlin-blueviolet
[pipeline-shield]: https://gitlab.com/sandlin/project_templates/common/groot/pipeline.svg
[pipeline-status]: https://gitlab.com/sandlin/project_templates/common/badges/groot/pipeline.svg
[code-coverage]: https://gitlab.com/sandlin/project_templates/common/badges/groot/coverage.svg
